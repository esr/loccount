#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause

# Massage a JSON traits table into an equivalent Go initializer.
# We do this in code generation, rather than reading in JSON
# at loccount startup, to make it faster.
#
# Two options:
#
# -f   Factor entries that can be shortened with De
# -g   Generate a Go initializer
# -j   Dump JSON after folding in parts entries with De references resolved.

# pylint: disable=line-too-long,missing-module-docstring,multiple-imports,invalid-name,missing-function-docstring,redefined-outer-name,consider-using-f-string

import json, sys, getopt, copy

try:
    (options, arguments) = getopt.getopt(sys.argv[1:], "fgj")
except getopt.GetoptError as e:
    print(e)
    sys.exit(1)
generate_go = False
generate_json = False
factor = False

for (switch, val) in options:
    if switch == "-f":
        factor = True
    elif switch == "-g":
        generate_go = True
    elif switch == "-j":
        generate_json = True

if len(arguments) == 0:
    sys.stderr.write("%s: at least one argument is required.\n" % sys.argv[0])
    sys.exit(1)

source = arguments[0]

if len(arguments) == 1:
    target = "/dev/stdout"
else:
    target = arguments[1]

parts = {}


def escapify(s):
    s = repr(s)[1:-1]
    s = s.replace(r'"', r"\"").replace(r"\'", r"'")
    return s


def go_dump(original):
    # Dump an entry in the form of a Go initializer
    entry = original.copy()
    if "Quirks" not in entry:
        entry["Quirks"] = ["nf"]
    entry["Quirks"] = " | ".join(entry["Quirks"])
    out = (
        '\t{Name:"%(Name)s",Ext:[]string{%(Ext)s},Bcl:"%(Bcl)s",Bct:"%(Bct)s",Wcl:"%(Wcl)s",quirks:%(Quirks)s,St:"%(St)s",Ml:[][2]string{%(Ml)s},verifier:%(Verifier)s},'
        % entry
    )
    return out.encode("ascii")


def json_dump(entry):
    # Dump an entry in the form of a JSON object with fields in a canonical order
    out = '{"Name":"%(Name)s"' % entry
    if "Ext" in entry:
        out += ',"Ext":['
        for ext in entry["Ext"]:
            out += '"' + escapify(ext) + '",'
        out = out[:-1] + "]"
    if "De" in entry and entry["De"]:
        out += ',"De":"%(De)s"' % entry
    if "Bcl" in entry and entry["Bcl"]:
        out += ',"Bcl":"%s","Bct":"%s"' % (
            escapify(entry["Bcl"]),
            escapify(entry["Bct"]),
        )
    if "Wcl" in entry and entry["Wcl"]:
        out += ',"Wcl":"%s"' % escapify(entry["Wcl"])
    if "St" in entry and entry["St"]:
        out += ',"St":"%s"' % escapify(entry["St"])
    if "Ml" in entry and entry["Ml"]:
        out += ',"Ml":['
        for ml in entry["Ml"]:
            out += '["' + escapify(ml[0]) + '","' + escapify(ml[1]) + '"],'
        out = out[:-1] + "]"
    if "Quirks" in entry and entry["Quirks"]:
        out += ',"Quirks":['
        for quirk in entry["Quirks"]:
            out += '"' + escapify(quirk) + '",'
        out = out[:-1] + "]"
    if "Verifier" in entry:
        out += ',"Verifier":"%(Verifier)s"' % entry
    out += "}"
    return bytes(out, "ascii")


def resolve(entry):
    # Expand De attributes
    if "De" in entry:
        proto = parts[entry["De"]]
        for k in ("Bcl", "Bct", "Wcl", "St"):
            if not entry.get(k) and k in proto:
                entry[k] = proto[k]
        if "Quirks" not in entry:
            entry["Quirks"] = []
        for f in proto["Quirks"]:
            if not f in entry["Quirks"]:
                entry["Quirks"].append(f)
    return entry


def is_superset_of(target, source):
    # Is target a superset of source, such that source could be shortened by using De?
    print("Comparing target %s with source %s" % (target, source))
    for prop in ("Bcl", "Bct", "Bcl", "Wcl", "St", "Ml", "Quirks", "Verifier"):
        tval = target.get(prop)
        if isinstance(tval, list):
            tval.sort()
        sval = source.get(prop)
        if isinstance(sval, list):
            sval.sort()
        print(
            "Property %s: target %s=%s source %s=%s"
            % (prop, target["Name"], tval, source["Name"], sval)
        )
        if sval == tval:
            continue
        if isinstance(sval, set) and isinstance(tval, set):
            if not sval > tval:
                return False
        elif sval and tval:  # Nonequal; and both non-nil
            return False
        elif sval:
            return False
    return True


with open(target, "wb") as wfp:
    with open(source, "r", encoding="ascii") as rfp:
        if generate_go:
            wfp.write(
                b"""\
// This file is generated - do not hand-hack!

package main

var genericLanguages = []genericLanguage{
"""
            )
        for line in rfp:
            if line[0] != "{":
                continue
            entry = json.loads(line)
            original = copy.deepcopy(
                entry
            )  # Shallow copy should suiffice here but does not.
            if "Ext" in entry:
                if generate_go:
                    entry["Ext"] = repr(entry["Ext"]).replace("'", '"')[1:-1]
            if "Ml" in entry and generate_go:
                entry["Ml"] = ",".join(
                    [
                        (
                            '[2]string{"%s","%s"}'
                            % (escapify(pair[0]), escapify(pair[1]))
                        )
                        for pair in entry["Ml"]
                    ]
                )
            else:
                entry["Ml"] = ""
            for k in ("Bcl", "Bct", "Wcl", "St"):
                if k not in entry:
                    entry[k] = ""
                else:
                    entry[k] = escapify(entry[k])
            if "Verifier" not in entry:
                entry["Verifier"] = "nil"
            # Every entry is a reusable part
            partname = entry["Name"]
            parts[partname] = {}
            for key in entry.keys():
                parts[partname][key] = entry[key]
            entry = resolve(entry)
            # Report generation
            if generate_go:
                if "Ext" in entry:
                    wfp.write(go_dump(entry) + b"\n")
            elif generate_json:
                wfp.write(json_dump(original) + b"\n")
            elif factor:
                for sourcename, sourceentry in parts.items():
                    if sourceentry["Name"] == entry["Name"]:
                        continue
                    if (
                        not sourceentry.get("De") or not original.get("De")
                    ) and is_superset_of(entry, sourceentry):
                        print("%s > %s" % (entry["Name"], sourcename))
        if generate_go:
            wfp.write(b"}\n")

# end
