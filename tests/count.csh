# csh: SLOC=7 LLOC=0
foreach number (one two three exit four)
  if ($number == exit) then
    echo reached an exit
    # Random comment as interruption
    continue
  endif
  echo $number
end
# Should count as 7 lines
