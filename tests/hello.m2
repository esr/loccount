(* Modula-2: SLOC=6 LLOC=4 *)
MODULE hello;

FROM InOut IMPORT writestring, writeln;

begin
  WriteString("Hello, world!");
  Writeln;
end hello.
