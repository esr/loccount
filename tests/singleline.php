<?php
// PHP: SLOC=4 LLOC=1

/*
Here is a multi-line comment
*/

# Single-line comments can also start with a hash character

function foo() {
  return '"';
}
